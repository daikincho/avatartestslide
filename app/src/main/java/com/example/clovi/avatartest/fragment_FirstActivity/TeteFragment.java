package com.example.clovi.avatartest.fragment_FirstActivity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.clovi.avatartest.CustomSwipeAdapter;
import com.example.clovi.avatartest.FirstActivity;
import com.example.clovi.avatartest.R;

/**
 * Created by clovi on 17/01/2017.
 */

public class TeteFragment extends Fragment {

    private Button boutton1_TeteFr;
    private View vue_fragement_tete;
    private ViewPager viewPager;
    private CustomSwipeAdapter adapter;

    public interface FragmentTouchlistener{
        void toastMessageFragTete();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vue_fragement_tete = inflater.inflate(R.layout.fragment_tete,container,false);
        frboutton();
        viewPager = (ViewPager) vue_fragement_tete.findViewById(R.id.view_pager);
        adapter = new CustomSwipeAdapter(getActivity());
        viewPager.setAdapter(adapter);
        return vue_fragement_tete;

    }
    //methode qui execute l'action lors de l'apppui sur le boutton, elle fait appel a l'activité maitre pour executer la methode qui execute une acion
    private void frboutton(){
        boutton1_TeteFr = (Button) vue_fragement_tete.findViewById(R.id.button_write_on_FirstActivity);
        boutton1_TeteFr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() instanceof FragmentTouchlistener) { //ici on verifie que l'activité mère a bien implémenté l'interface FramentTouchlistener
                    ((FragmentTouchlistener) getActivity()).toastMessageFragTete();
                }

            }
        });
    }
}


