package com.example.clovi.avatartest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import android.support.v4.view.ViewPager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.clovi.avatartest.fragment_FirstActivity.TeteFragment;

/**
 * Created by clovi on 17/01/2017.
 */

public class FirstActivity extends Activity implements TeteFragment.FragmentTouchlistener{
    private FrameLayout frTete_container;//creation d'une propriété qui va servir de vue
    ViewPager viewPager;
    CustomSwipeAdapter adapter;


    //un bundle est une classe qui contient des données qu'on veux faire véhiculer entre les activité/fragment
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);// code/generate/Override Methods/
        setContentView(R.layout.activity_first);//²contentview : contenu de la vue ;   activity_new est une vue créee depuis res/Layout/new/Layout Resource File, on assoce la vue a la class
        frTete_container =(FrameLayout) findViewById(R.id.fragmentTete);//liaison entre le frament et une activité
        //a ton appel tu crée le fragment (appel d'une methode)
        addFragmentTete();

      /*  viewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new CustomSwipeAdapter(this);
        viewPager.setAdapter(adapter);*/
    }
    public void addFragmentTete(){
        FragmentManager manager = getFragmentManager(); //creation d'un manager
        FragmentTransaction tx = manager.beginTransaction();//mise en place d'une transaction
        tx.replace(R.id.fragmentTete, new TeteFragment());
        tx.commit(); //la mise en place d'une transaction oblige un commit
    }

    public void toastMessageFragTete(){
        Toast.makeText(this, "Le boutton1 du fragmentTete a été cliqué",Toast.LENGTH_SHORT).show();
    }

}
