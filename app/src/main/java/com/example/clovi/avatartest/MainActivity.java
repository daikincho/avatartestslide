package com.example.clovi.avatartest;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.R.attr.button;

public class MainActivity extends AppCompatActivity {



    private Button bouton1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
       bouton1 = (Button) findViewById(R.id.button_page_firstA);
       bouton1.setBackgroundColor(Color.BLUE);
        bouton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,FirstActivity.class);//on crée une instance qui cible l'activité NextActivity "lorsque quelqu'un clique sur ce boutton tu fais "a partir de moi, tu vas sur cette page (class)""
                startActivity(intent);//on demarre l'activité
            }
        });
    }
    @OnClick (R.id.button_page_firstA)
    public void onclickMethode(){
        Toast.makeText(this,"salutttttt",Toast.LENGTH_SHORT).show();
    }

}
